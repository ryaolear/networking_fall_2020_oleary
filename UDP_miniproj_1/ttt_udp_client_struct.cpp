/**
 * In-class demonstrated UDP client example.
 */

#include <iostream>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "udp_utils.h"
#include "tictactoe.h"

using namespace std;

/**
 *
 * Dead simple UDP client example. Reads in IP PORT DATA
 * from the command line, and sends DATA via UDP to IP:PORT.
 *
 * e.g., ./udpclient 127.0.0.1 8888 this_is_some_data_to_send
 *
 * @param argc count of arguments on the command line
 * @param argv array of command line arguments
 * @return 0 on success, non-zero if an error occurred
 */
int main(int argc, char *argv[]) {
    // Alias for argv[1] for convenience
    char *ip_string;
    // Alias for argv[2] for convenience
    char *port_string;
    // Alias for argv[3] for convenience
    char *data_string;
    // Port to send UDP data to. Need to convert from command line string to a number
    unsigned int port;
    // The socket used to send UDP data on
    int udp_socket;
    // Variable used to check return codes from various functions
    int ret;

    // Test struct to show how to send things over the network
    struct GetGameMessage to_send;
    struct GameSummaryMessage to_receive;
    struct GameResultMessage to_send2;
    struct TTTMessage to_receive2;

    // IPv4 structure representing and IP address and port of the destination
    struct sockaddr_in dest_addr;
    /* buffer to use for receiving data */
    static char recv_buf[4096];
    /* recv_addr is the client who is talking to us */
    struct sockaddr_in recv_addr;
    /* recv_addr_size stores the size of recv_addr */
    socklen_t recv_addr_size;
    /* buffer to use for sending data */
    static char send_buf[4096];
    /* variable to hold return values from network functions */


    struct GameResultMessage get_game_result;


    // Set dest_addr to all zeroes, just to make sure it's not filled with junk
    // Note we could also make it a static variable, which will be zeroed before execution
    memset(&dest_addr, 0, sizeof(struct sockaddr_in));

    // Note: this needs to be 4, because the program name counts as an argument!
    if (argc < 4) {
        std::cerr << "Please specify IP PORT DATA as first three arguments." << std::endl;
        return 1;
    }
    // Set up variables "aliases"
    ip_string = argv[1];
    port_string = argv[2];
    data_string = argv[3];

    // Create the UDP socket.
    // AF_INET is the address family used for IPv4 addresses
    // SOCK_DGRAM indicates creation of a UDP socket
    udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    // Make sure socket was created successfully, or exit.
    if (udp_socket < 0) {
        handle_error("UDP socket creation has successfully failed.");
    }

    // inet_pton converts an ip address string (e.g., 1.2.3.4) into the 4 byte
    // equivalent required for using the address in code.
    // Note that because dest_addr is a sockaddr_in (again, IPv4) the 'sin_addr'
    // member of the struct is used for the IP
    // Check whether the specified IP was parsed properly. If not, exit.
    // Convert the port string into an unsigned integer.
    // sscanf is called with one argument to convert, so the result should be 1
    // If not, exit.
    // Set the address family to AF_INET (IPv4)
    // Set the destination port. Use htons (host to network short)
    // to ensure that the port is in big endian format

    ret = convert_ip_port_to_sockaddr_in(ip_string, port_string, &dest_addr);
    if (ret == -1) {
        handle_error("ip/port conversion failed");
    }


    // Send the data to the destination.
    // Note 1: we are sending strlen(data_string) (don't include the null terminator)
    // Note 2: we are casting dest_addr to a struct sockaddr because sendto uses the size
    //         and family to determine what type of address it is.
    // Note 3: the return value of sendto is the number of bytes sent
    //ret = sendto(udp_socket, data_string, strlen(data_string), 0,
    //             (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in));

    to_send.hdr.type = htons(ClientGetGame);
    to_send.hdr.len = htons(sizeof(struct GetGameMessage));
    to_send.client_id = htons(12345);

    //Now we want to send to_send struct instead
    ret = sendto(udp_socket, &to_send, sizeof(struct GetGameMessage), 0,
                 (struct sockaddr *) &dest_addr, sizeof(struct sockaddr_in));

    // need a recvfrom here, then parse message type, etc.
    recv_addr_size = sizeof(struct sockaddr_in);
    ret = recvfrom(udp_socket, recv_buf, 2047, 0, (struct sockaddr *) &recv_addr, &recv_addr_size);

    if (ret <= 0) {
        handle_error("recvfrom failed for some reason");
        close(udp_socket);
        return 1;
    }
    std::cout << "Received " << ret << " bytes of data." << std::endl;

    int gamestate = 0;
        /* find game values;
          gamestate possible values:
          1 = x win
          2 = o win
          3 = draw
          4 = impossible game
          0 = error while testing
          */
    //prepare return values
    to_send2.result = 0;
    if (ret >= sizeof(struct GameSummaryMessage)) {
        memcpy(&to_receive, recv_buf, sizeof(struct GameSummaryMessage));
        if (to_receive.hdr.type == ServerInvalidRequestReply) {
            handle_error("Invalid request");
            close(udp_socket);
            return 1;
        }

        std::cout << "received game " << to_receive.game_id << std::endl;
        to_receive.client_id = ntohs(to_receive.client_id);
        to_receive.game_id = ntohs(to_receive.game_id);
        to_receive.x_positions = ntohs(to_receive.x_positions);
        to_receive.o_positions = ntohs(to_receive.o_positions);
        to_send2.game_id = ntohs(to_receive.game_id);

        std::cout << "received GameSummaryMessage of type " << to_receive.hdr.type << " length "
                  << to_receive.hdr.len << " game_id " << to_receive.game_id << std::endl;
        std::cout << to_receive.x_positions << " are the x positions, and " << to_receive.o_positions
                  << " are the o positions" << std::endl;
        int tobinaryx[9];
        int tobinaryo[9];
        int i = 0;
        int j = 0;
        uint16_t tempx = to_receive.x_positions;
        uint16_t tempo = to_receive.o_positions;

        //convert x values to position binary (will be in reverse order)
        while (tempx > 0) {
            tobinaryx[i] = tempx % 2;
            tempx = tempx / 2;
            i++;
            //adds any additional 0s that would otherwise be omitted
            while (i < 9 && tempx <= 0) {
                tobinaryx[i] = 0;
                i++;
            }
        }
        //convert o values to position binary (will be in reverse order)
        while (tempo > 0) {
            tobinaryo[j] = tempo % 2;
            tempo = tempo / 2;
            j++;

            while (j < 9 && tempo <= 0) {
                tobinaryo[j] = 0;
                j++;
            }

        }
        int posbinaryx[9];
        int posbinaryo[9];
        int k = 0;
        int l = 0;
        char board[9];
        //print x positions and reverse them
        std::cout << "now verifying x values " << std::endl;

        for (int reverse = i - 1; reverse >= 0; reverse--) {
            std::cout << tobinaryx[reverse];
            posbinaryx[k] = tobinaryx[reverse];
            k++;
        }

        std::cout << std::endl;
        std::cout << "now verifying o values " << std::endl;


        //print o positions and reverse them
        for (int reverse = j - 1; reverse >= 0; reverse--) {
            std::cout << tobinaryo[reverse];
            posbinaryo[l] = tobinaryo[reverse];
            l++;
        }
        //verify newly made binary arrays
        std::cout << std::endl;
        std::cout << "now verifying table values " << std::endl;

        int m = 0;
        for (m = 0; m < 9; m++) {
            std::cout << posbinaryx[m];
        }
        std::cout << std::endl;
        m = 0;
        for (m = 0; m < 9; m++) {
            std::cout << posbinaryo[m];
        }
        std::cout << std::endl;
        //first check for impossible game
        m = 0;

        for (int r = 0; r < 9; r++) {
//          std::cout << "value for binaryo " << posbinaryo[r] <<std::endl;
//          std::cout << "value for binaryx " << posbinaryx[r] <<std::endl;
//          std::cout << "r value " << r <<std::endl;
//          std::cout << "gamestate " << gamestate <<std::endl;


//          std::cout << "0r value "<< r <<" has " << posbinaryo[r] << " and " << posbinaryx[r] << " as their o and x value" << std::endl;
//          std::cout << "0r value "<< r <<" has " << posbinaryo[r] << " and " << posbinaryx[r] << " as their o and x value" << std::endl;

            //tests whether or not there's any overlap
            if ((posbinaryx[r] == posbinaryo[r]) && ((posbinaryo[r] == 1) && (posbinaryx[r] == 1))) {
                gamestate = 4;
//              std::cout << "gamestate " << gamestate <<std::endl;

            }

            if (((posbinaryo[r] == 1) && (posbinaryx[r] == 0)) && !(gamestate == 4)) {
                board[r] = char('o');
//              std::cout << "board value " << board[r] <<std::endl;

            }

            if (((posbinaryo[r] == 0) && (posbinaryx[r] == 1)) && !(gamestate == 4)) {
                board[r] = char('x');
//              std::cout << "board value " << board[r] <<std::endl;

            }

            if (((posbinaryo[r] == 0) && (posbinaryx[r] == 0)) && !(gamestate == 4)) {
                board[r] = char('.');
//              std::cout << "board value " << board[r] <<std::endl;
            }
//          std::cout << "board r" << board[r] << std::endl;

        }
//        std::cout << "gamestate " << gamestate << std::endl;
//        std::cout << "board at 0 " << board[0] << std::endl;

        int w = 0;
        char coltest[3];
        char rowtest[3];
         for (int f = 0; f < 9; f++) {
//                coltest[w] = board[f];
//                cout << "coltest " << std::string(1, coltest[w]) << " " << f << endl;
//                cout << "board value " << std::string(1, board[f]) << " " << f << endl;

            }

        //check rows for a win
        std::cout << board[0] << " | " << board[1] << " | " << board[2] << std::endl;
        std::cout << board[3] << " | " << board[4] << " | " << board[5] << std::endl;
        std::cout << board[6] << " | " << board[7] << " | " << board[8] << std::endl;

        if ((board[0] == board[1]) && (board[1] == board[2])) {
            //sets gamestate = impossible if there's already a win
            if ((gamestate == 1)||(gamestate == 2)) {
                gamestate = 3;
                std::cout << "gamestate " << gamestate << std::endl;
            }
            else if (board[0] == 'x') {
                gamestate = 1;
                std::cout << "gamestate " << gamestate << std::endl;

            } else if (board[0] == 'o') {
                gamestate = 2;
                std::cout << "gamestate " << gamestate << std::endl;

            }
                //easy way to make sure that blank spaces don't need to be considered
            else if (board[0] == ' ') {}
            else {
                gamestate = 4;
                std::cout << "gamestate " << gamestate << std::endl;

            }
        }
        if ((board[3] == board[4]) && (board[4] == board[5])) {
            //sets gamestate = impossible if there's already a win
//            std::cout << "test " << std::endl;

            if ((gamestate == 1)||(gamestate == 2)) {
                gamestate = 3;
                std::cout << "gamestate " << gamestate << std::endl;
            }
            else if (board[3] == 'x') {
                gamestate = 1;
                std::cout << "gamestate " << gamestate << std::endl;

            } else if (board[3] == 'o') {
                gamestate = 2;
                std::cout << "gamestate " << gamestate << std::endl;

            }
                //easy way to make sure that blank spaces don't need to be considered
            else if (board[3] == ' ') {}
            else {
                gamestate = 4;
                std::cout << "gamestate " << gamestate << std::endl;

            }
        }
        if ((board[6] == board[7]) && (board[7] == board[8])) {
            //sets gamestate = impossible if there's already a win
            if ((gamestate == 1)||(gamestate == 2)) {
                gamestate = 3;
                std::cout << "gamestate " << gamestate << std::endl;
            }
            else if (board[6] == 'x') {
                gamestate = 1;
                std::cout << "gamestate " << gamestate << std::endl;

           } else if (board[6] == 'o') {
                gamestate = 2;
                std::cout << "gamestate " << gamestate << std::endl;

            }
                //easy way to make sure that blank spaces don't need to be considered
            else if (board[6] == ' ') {}
            else {
                gamestate = 4;
                std::cout << "gamestate " << gamestate << std::endl;

            }
        }

        //check columns for a win
        if ((board[0] == board[3]) && (board[3] == board[6])) {
            if ((gamestate == 1)||(gamestate == 2)) {
                gamestate = 3;
                std::cout << "gamestate " << gamestate << std::endl;
            }
                else if (board[0] == 'x') {
                gamestate = 1;
                std::cout << "gamestate " << gamestate << std::endl;

            } else if (board[0] == 'o') {
                gamestate = 2;
                std::cout << "gamestate " << gamestate << std::endl;

            }
                //easy way to make sure that blank spaces don't need to be considered
            else if (board[0] == ' ') {}
            else {
                gamestate = 4;
                std::cout << "gamestate " << gamestate << std::endl;

            }
            //sets gamestate = impossible if there's already a win
        }
        if ((board[1] == board[4]) && (board[4] == board[7])) {
//            std::cout << "test col" << std::endl;

            if ((gamestate == 1)||(gamestate == 2)) {
                gamestate = 3;
                std::cout << "gamestate " << gamestate << std::endl;}
            else if (board[1] == 'x') {
                gamestate = 1;
                std::cout << "gamestate " << gamestate << std::endl;

            } else if (board[1] == 'o') {
                gamestate = 2;
                std::cout << "gamestate " << gamestate << std::endl;

            }
                //easy way to make sure that blank spaces don't need to be considered
            else if (board[1] == ' ') {}
            else {
                gamestate = 4;
                std::cout << "gamestate " << gamestate << std::endl;

            }
            //sets gamestate = impossible if there's already a win
        }
        if ((board[2] == board[5]) && (board[5] == board[8])) {
            if ((gamestate == 1)||(gamestate == 2)) {
                gamestate = 3;
                std::cout << "gamestate " << gamestate << std::endl;

            } else if (board[2] == 'x') {
                gamestate = 1;
                std::cout << "gamestate " << gamestate << std::endl;

            } else if (board[2] == 'o') {
                gamestate = 2;
                std::cout << "gamestate " << gamestate << std::endl;
            }
                //easy way to make sure that blank spaces don't need to be considered
            else if (board[2] == ' ') {}
            else {
                gamestate = 4;
                std::cout << "gamestate " << gamestate << std::endl;

            }
            //sets gamestate = impossible if there's already a win
        }

        //check diagonals
        if ((board[0] == board[4]) && (board[4] == board[8])) {
            //sets gamestate = impossible if there's already a win
            if ((gamestate == 1)||(gamestate == 2)) {
                gamestate = 3;
                std::cout << "gamestate " << gamestate << std::endl;
            }
            else if (board[0] == 'x') {
                gamestate = 1;
                std::cout << "gamestate " << gamestate << std::endl;

            } else if (board[0] == 'o') {
                gamestate = 2;
                std::cout << "gamestate " << gamestate << std::endl;

            }
                //easy way to make sure that blank spaces don't need to be considered
            else if (board[0] == ' ') {}
            else {
                gamestate = 4;
                std::cout << "gamestate " << gamestate << std::endl;

            }
        }
        if ((board[2] == board[4]) && (board[4] == board[6])) {
            if ((gamestate == 1)||(gamestate == 2)) {
                gamestate = 3;
                std::cout << "gamestate " << gamestate << std::endl;
            }
            else if (board[2] == 'x') {
                gamestate = 1;
                std::cout << "gamestate " << gamestate << std::endl;

            } else if (board[2] == 'o') {
                gamestate = 2;
                std::cout << "gamestate " << gamestate << std::endl;
            }
                //easy way to make sure that blank spaces don't need to be considered
            else if (board[2] == ' ') {}
            else {
                gamestate = 4;
                std::cout << "gamestate " << gamestate << std::endl;

            }
            //sets gamestate = impossible if there's already a win
        }

        //if no winning rows/columns, and no error found, sets gamestate = 3, cats game

      //  std::cout << "gamestate " << gamestate << std::endl;
    } else {
        std::cout << "received, but not game summary " << to_receive.x_positions << std::endl;
    }

    if (gamestate == 0) {
        gamestate = 3;
    }

    if (gamestate == 1)
    {
        to_send2.result=htons(X_WIN);
    }
    else if (gamestate == 2)
    {
        to_send2.result=htons(O_WIN);
    }
    else if (gamestate == 3)
    {
        to_send2.result=htons(CATS_GAME);
    }
    else if (gamestate == 4)
    {
        to_send2.result=htons(INVALID_BOARD);
    }
    else
    {
        handle_error("read failed");
        close(udp_socket);
        return 1;
    }
//    to_send2.game_id=htons(to_receive.game_id);
    to_send2.hdr.type=htons(ClientResult);
    to_send2.hdr.len= htons(sizeof(GameResultMessage));

    //Now we want to send to_send struct instead
    ret = sendto(udp_socket, &to_send2, sizeof(struct GameResultMessage), 0,
                 (struct sockaddr *) &dest_addr, sizeof(struct sockaddr_in));


    // Check if send worked, clean up and exit if not.
    if (ret <= 0) {
        handle_error("Sendto failed");
        close(udp_socket);
        return 1;
    }

    std::cout << "Sent " << ret << " bytes out." << std::endl;

    recv_addr_size = sizeof(struct sockaddr_in);
    ret = recvfrom(udp_socket, recv_buf, 2047, 0, (struct sockaddr *) &recv_addr, &recv_addr_size);

    if (ret <= 0) {
        handle_error("recvfrom failed for some reason");
        close(udp_socket);
        return 1;
    }
    if (ret >= sizeof(struct TTTMessage)) {
        memcpy(&to_receive2, recv_buf, sizeof(struct TTTMessage));
        if (htons(to_receive2.type) == ServerInvalidRequestReply) {
            handle_error("Invalid request");
            close(udp_socket);
            return 1;
        }
//        if(htons(to_receive2.type)== ServerClientResultCorrect )
//        {
//            std::cout << "true" << std::endl;
//        }
//        else if(htons(to_receive2.type) == ServerClientResultIncorrect)
//        {
//            std::cout << "false" << std::endl;
//        }
//        else if(htons(to_receive2.type) == ServerInvalidRequestReply)
//        {
//            std::cout << "nope" << std::endl;
//        }

        std::cout << "Received " << ret << " bytes of data from Ip Address "<<argv[1]<<" : " << argv[2] << endl;
        std::cout << " Result was " << ntohs(to_receive2.type) << std::endl;
    }
    else{
        std::cout << "Received no TTTMessage" << std::endl;
    }
//    if (ret >= sizeof(struct GameSummaryMessage)) {
//      memcpy(&to_receive, recv_buf, sizeof(struct GameSummaryMessage));
//      to_receive.type = ntohs(to_receive.type);
//      to_receive.len = ntohs(to_receive.len);
//      std::cout << "received message type " << to_receive.type << " length " << to_receive.len << std::endl;
//
//      if (((to_receive.type == ServerClientResultCorrect) || (to_receive.type == ServerClientResultIncorrect))
//      && (ret <= sizeof(struct GameSummaryMessage)))
//      {
//        memcpy(&get_game_result, recv_buf, sizeof(struct GameResultMessage));
//        get_game_result.hdr.type = ntohs(get_game_result.hdr.type);
//        get_game_result.hdr.len = ntohs(get_game_result.hdr.len);
//        get_game_result.result = ntohs(get_game_result.result);
//
//        std::cout << "received GameResultMessage, type " << get_game_message.hdr.type << " length "
//                  << get_game_message.hdr.len << " result " << get_game_message.result << std::endl;
//      }
//    }*/

//    char * boardposx = (char*)(&x_pos);
//    char * boardposy = (char*)(&y_pos);


    close(udp_socket);
    return 0;
}