//
// Created by Nathan Evans
//
#include <iostream>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/poll.h>
#include <netdb.h>
#include "tcp_utils.h"
#include "tcp_chat.h"
#include <sys/time.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <vector>
#include "tcp_chat.h"
#ifndef SOCK_NONBLOCK
#define SOCK_NONBLOCK O_NONBLOCK
#endif

bool quit = false;

std::string get_nickname() {
    std::string nickname;
    std::cout << "Enter chat nickname: ";
    std::getline(std::cin, nickname);
    return nickname;
}

std::string get_message() {
    std::string msg;
    std::cout << "Enter chat message to send, or quit to quit: ";
    std::getline(std::cin, msg);
    //std::cerr << "Got input " << msg << " from user" << std::endl;
    return msg;
}

// Handler for when ctrl+c is pressed.
// Just set the global 'stop' to true to shut down the server.
void handle_ctrl_c(int the_signal) {
    std::cout << "Handled sigint\n";
    quit = true;
}

/**
 *
 * Chat client example. Reads in HOST PORT
 *
 * e.g., ./tcpchatclient 127.0.0.1 8888
 *
 * @param argc count of arguments on the command line
 * @param argv array of command line arguments
 * @return 0 on success, non-zero if an error occurred
 */
int main(int argc, char *argv[]) {
    // Alias for argv[1] for convenience
    char *ip_string;
    // Alias for argv[2] for convenience
    char *port_string;

    // The socket used to connect/send/receive data to the TCP server
    int tcp_socket;
    // Variable used to check return codes from various functions
    int ret;

    /* buffer to use for receiving data */
    static char recv_buf[2048];

    struct sockaddr_in result;

    struct sockaddr_in dest_addr;
    memset(&dest_addr, 0, sizeof(struct sockaddr_in));

    std::string nickname;

    // Note: this needs to be 3, because the program name counts as an argument!
    if (argc < 3) {
        std::cerr << argc << std::endl;
        std::cerr << "Please specify HOST PORT as first two arguments." << std::endl;
        return 1;
    }
    // Set up variables "aliases"
    ip_string = argv[1];
    port_string = argv[2];

    // Signal handler setup, done for you! This allows you to hit ctrl+c when running from the command line
    // This will set the global quit variable to true and allow you to cleanly shut down from the program
    // E.g., send a disconnect message to the server and then close the TCP connection
    struct sigaction ctrl_c_handler;
    ctrl_c_handler.sa_handler = handle_ctrl_c;
    sigemptyset(&ctrl_c_handler.sa_mask);
    ctrl_c_handler.sa_flags = 0;
    sigaction(SIGINT, &ctrl_c_handler, NULL);

    // Create the TCP socket.
    // AF_INET is the address family used for IPv4 addresses
    // SOCK_STREAM indicates creation of a TCP socket
    tcp_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    // Make sure socket was created successfully, or exit.
    if (tcp_socket == -1) {
        std::cerr << "Failed to create tcp socket!" << std::endl;
        std::cerr << strerror(errno) << std::endl;
        return 1;
    }

    struct addrinfo hints;
    struct addrinfo *results;
    struct addrinfo *results_it;
    memset(&hints, 0, sizeof(struct addrinfo));
    // TODO: Get the IP address in binary format using get getaddrinfo!

    hints.ai_family = AF_INET;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_socktype = SOCK_STREAM;
    ret = getaddrinfo(ip_string, port_string, &hints, &results);
    std::cout << "raw getaddrinfo " << ret << std::endl;
    if (ret != 0) {
        perror("getaddrinfo");
        return -1;
    }

    // TODO: Connect to TCP Chat Server using connect()

    results_it = results;
    std::cout << "raw results_it " << results_it << " " << results << std::endl;

    ret = -1;
    while (results_it != NULL) {
        std::cout << "Attempting to connect to " <<
                  printable_address((struct sockaddr_storage *) results_it->ai_addr, results_it->ai_addrlen)
                  << std::endl;
        ret = connect(tcp_socket, results_it->ai_addr, results_it->ai_addrlen);
        if (ret == 0) // Success
        {
            std::cout << results_it << std::endl;
            break;
        }
        ret = -1;
        perror("connect");
        results_it = results_it->ai_next;
    }
    freeaddrinfo(results);

    if (ret == -1) {
        handle_error("connect failed");
    }

    ret = convert_ip_port_to_sockaddr_in(ip_string, port_string, &result);
    if (ret == -1) {
        std::cerr << "ip/port conversion failed" << std::endl;
    }
    if (ret != 0) {
        std::cerr << "Failed to connect to remote server!" << std::endl;
        std::cerr << ret << std::endl;
        std::cerr << strerror(errno) << std::endl;
        close(tcp_socket);
        return 1;
    }
    // TODO: Send connect message
    // Fill in client_message and send to the server
    nickname = get_nickname();

    struct ChatClientMessage client_message;
    memset(&client_message, 0, sizeof(client_message));
    client_message.type = CLIENT_CONNECT;
    client_message.nickname_len = 0;
    client_message.data_length = sizeof(nickname);

    ret = send(tcp_socket, &client_message, sizeof(client_message), 0);

    if (ret <= 0) {
        handle_error("client Send failed");
        close(tcp_socket);
        return 1;
    }

    struct ChatClientMessage nickname_message;
    memset(&nickname_message, 0, sizeof(nickname_message));
    nickname_message.type = CLIENT_SET_NICKNAME;
    nickname_message.nickname_len = 0;
    nickname_message.data_length = nickname.length();
    ret = send(tcp_socket, &nickname_message, sizeof(nickname_message), 0);
    if (ret <= 0) {
        handle_error("nickname Send failed");
        close(tcp_socket);
        return 1;
    }

    // TODO: Send nickname message

    // Now enter a loop to send the chat messages from this client to the server
    std::string next_message;
    next_message = get_message();

    while ((next_message != "quit") && (quit == false)) {
        if (next_message == ("LIST")) {
            std::cout << " Listing " << std::endl;
            struct ChatClientMessage list_message;
            memset(&list_message, 0, sizeof(list_message));
            list_message.type = CLIENT_GET_MEMBERS;
            list_message.nickname_len = 0;
            list_message.data_length = sizeof(nickname);
            ret = send(tcp_socket, &list_message, sizeof(list_message), 0);
            if (ret <= 0) {
                handle_error("list Send failed");
                close(tcp_socket);
                return 1;
            }
        } else if (next_message.front() == '\\') {
            struct ChatClientMessage direct_message;
            memset(&direct_message, 0, sizeof(direct_message));
            direct_message.type = CLIENT_SEND_DIRECT_MESSAGE;
            direct_message.nickname_len = 0;
            direct_message.data_length = sizeof(nickname);
            int msglen = next_message.length();
            char msg_buf[sizeof(struct ChatMonMsg)+(nickname).length()];
            std::string combmsg;
            std::string typemsg =std::to_string(direct_message.type);
            combmsg = (typemsg + nickname);
            int offset = 0;
            memcpy(&msg_buf[offset], &direct_message, sizeof(struct ChatMonMsg));
            offset +=sizeof(struct ChatMonMsg);
            memcpy(&msg_buf[offset], nickname.c_str(), (nickname.length()));
            ret = send(tcp_socket, msg_buf, sizeof(msg_buf), 0);
            std::cout << "msg_buf now contains " << msg_buf << " data" << std::endl;

//
//            std::cout << "ret " << ret << std::endl;
//            std::cout << "msg_buf " << msg_buf << std::endl;
//            std::cout << "next_message " << next_message << std::endl;
            ret = send(tcp_socket, msg_buf, sizeof(msg_buf), 0);

            ret = send(tcp_socket, &direct_message, sizeof(direct_message), 0);
            if (ret <= 0) {
                handle_error("direct message failed");
                close(tcp_socket);
                return 1;
            }
        } else {

            struct ChatClientMessage normal_message;
            memset(&normal_message, 0, sizeof(normal_message));
            normal_message.type = 13;
            normal_message.nickname_len = 0;
            normal_message.data_length = sizeof(nickname);


            int msglen = next_message.length();
            char msg_buf[normal_message.data_length+normal_message.nickname_len+msglen+1];
            std::string combmsg;
            std::string typemsg =std::to_string(normal_message.type);
            combmsg = (typemsg + nickname + next_message);
            strcpy(msg_buf, combmsg.c_str());
            std::cout << "msg_buf now contains " << msg_buf << "  data" << std::endl;

//
//            std::cout << "ret " << ret << std::endl;
//            std::cout << "msg_buf " << msg_buf << std::endl;
//            std::cout << "next_message " << next_message << std::endl;
            ret = send(tcp_socket, msg_buf, sizeof(msg_buf), 0);

            std::cout << "sent " << ret << " bytes of data" << std::endl;

            if (ret <= 0) {
                handle_error(" message failed");
                close(tcp_socket);
                return 1;
            }
            std::cout << "ret " << ret << std::endl;
            std::cout << "Sending message " << next_message << std::endl;
            std::cout << ip_string << std::endl;


        }
        // TODO: parse command from next_message, either a regular message, a direct message, or a LIST message
        //       then send to the server the correct message type and data based on that

        next_message = get_message();
    }

    // TODO: build and send a client disconnect message to the server here


    close(tcp_socket);
    return 0;
}