// Created by Nathan Evans
//
//
#include <iostream>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/poll.h>
#include <netdb.h>
#include "tcp_utils.h"
#include "tcp_chat.h"
#include <sys/time.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <vector>

#ifndef SOCK_NONBLOCK
#define SOCK_NONBLOCK O_NONBLOCK
#endif

// Variable used to shut down the monitor when ctrl+c is pressed.
static bool stop = false;

// Handler for when ctrl+c is pressed.
// Just set the global 'stop' to true to shut down the server.
void handle_ctrl_c(int the_signal) {
    std::cout << "Handled sigint\n";
    stop = true;
}

/**
 * TCP chat monitor. Connects to a chat server and
 * simply prints out data to the client until it quits.
 *
 * e.g., ./tcpchatmon 127.0.0.1 8888
 *
 * @param argc count of arguments on the command line
 * @param argv array of command line arguments
 * @return 0 on success, non-zero if an error occurred
 */
struct Client{

    struct sockaddr_in dest_addr;
    socklen_t dest_addr_len;
    int client_socket;
    int poll_index;
    char recv_buf[2048];
    char send_buf[2048];
    int bytes_received;
    int bytes_to_send;
    struct sockaddr_in client_address;
    socklen_t client_address_len;
};
int timeout = 500;

bool closedsocket = true;

int process_data(struct Client *client) {
    struct ChatClientMessage clientmsg;
    struct ServerErrorMessage errormsg;
    struct ChatMonMsg monitormsg;

    if (client->bytes_received == 0)
        return 0;

    if (client->bytes_received >= sizeof(struct ChatClientMessage)) {
        memcpy(&clientmsg, client->recv_buf, sizeof(struct ChatClientMessage));
        clientmsg.type = ntohs(clientmsg.type);
        clientmsg.data_length = ntohs(clientmsg.data_length);

        if ((clientmsg.type == CLIENT_SEND_MESSAGE) && (clientmsg.data_length == sizeof(struct ChatClientMessage))
            && (client->bytes_received >= sizeof(struct ChatClientMessage))) {
            memcpy(&monitormsg, client->recv_buf, sizeof(struct ChatMonMsg));
            monitormsg.data_len = htons(monitormsg.data_len);
            std::cout << "Received GetGameMessage from client id: " << monitormsg.data_len << std::endl;

            clientmsg.type = htons(CLIENT_SEND_MESSAGE);
            clientmsg.data_length = htons(sizeof(struct ChatClientMessage));
            clientmsg.nickname_len = htons(0);
            if (client->bytes_to_send == 0) {
                memcpy(client->send_buf, &clientmsg, sizeof(struct ChatClientMessage));
                client->bytes_to_send = sizeof(struct ChatClientMessage);
                client->bytes_received = 0;
            }
        }
    }
    return 0;
}

int main(int argc, char *argv[]) {
    int len, rc, on = 1;
    int desc_ready, end_server = 0, compress_array = 0;
//    int    timeout;
    struct pollfd fds[200];
    int nfds = 1, current_size = 0, i, j;

    // Alias for argv[1] for convenience
    char *ip_string;
    // Alias for argv[2] for convenience
    char *port_string;
    // Alias for argv[3] for convenience
    char *nickname;
    // The socket used to connect/send/recv data to the TCP chat server
    int tcp_socket;
    int new_client_socket;
    unsigned int port;
    std::vector<struct Client *> client_list;
    struct Client * temp_client;
    struct sockaddr_in client_address;
    socklen_t client_address_len;
    static char recv_buf[2048];

    /* recv_addr is the client who is talking to us */
    struct sockaddr_in recv_addr;
    /* recv_addr_size stores the size of recv_addr */
    socklen_t recv_addr_size;
    /* buffer to use for sending data */
    static char send_buf[2048];
    /* variable to hold return values from network functions */

    // IPv4 structure representing and IP address and port of the destination

    // Set dest_addr to all zeroes, just to make sure it's not filled with junk
    // Note we could also make it a static variable, which will be zeroed before execution

    // Signal handler to deal with quitting the program appropriately
    struct sigaction ctrl_c_handler;
    ctrl_c_handler.sa_handler = handle_ctrl_c;
    sigemptyset(&ctrl_c_handler.sa_mask);
    ctrl_c_handler.sa_flags = 0;
    sigaction(SIGINT, &ctrl_c_handler, NULL);
    
    int ret;

    // Note: this needs to be 3, because the program name counts as an argument!
    if (argc < 3) {
        std::cerr << "Please specify server HOST PORT [NICKNAME] as arguments1." << std::endl;

        return 1;
    }
    std::string test;

    nickname = NULL;

    // Indicates that a nickname was provided for the monitor (for direct messages)
    if (argc == 4) {
        nickname = argv[3];
    }

    // Set up variables "aliases"
    ip_string = argv[1];
    port_string = argv[2];

    // Create the TCP socket.
    // AF_INET is the address family used for IPv4 addresses
    // SOCK_STREAM indicates creation of a TCP socket
    tcp_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
//    closedsocket = false;
    if (tcp_socket < 0) {
        perror("socket() failed");
        exit(-1);
    }
    int flags;
    flags = fcntl(tcp_socket, F_GETFL, 0);
    if (flags == -1) {
        perror("fcntl");
        return -1;
    }

    ret = fcntl(tcp_socket, F_SETFL, flags | SOCK_NONBLOCK);
    if (ret == -1) {
        perror("fcntl");
        return -1;
    }


    struct addrinfo hints;
    struct addrinfo *results;
    struct addrinfo *results_it;
    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family = AF_INET;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_socktype = SOCK_STREAM;
    ret = getaddrinfo(ip_string, port_string, &hints, &results);
    if (ret != 0) {
        perror("getaddrinfo");
        return -1;
    }
    results_it = results;

    ret = -1;
    while (results_it != NULL) {
        std::cout << "Attempting to bind to " <<
                  printable_address((struct sockaddr_storage *) results_it->ai_addr, results_it->ai_addrlen)
                  << std::endl;
        ret = bind(tcp_socket, results_it->ai_addr, results_it->ai_addrlen);
        if (ret == 0) // Success
        {
            std::cout << results_it << std::endl;
            break;
        }
        std::cout << "ret " << ret << " data" << std::endl;

        ret = -1;
        perror("bind");
        results_it = results_it->ai_next;
    }
    results_it = results;

    ret = -1;
    while (results_it != NULL) {
        std::cout << "Attempting to connect to " <<
                  printable_address((struct sockaddr_storage *) results_it->ai_addr, results_it->ai_addrlen)
                  << std::endl;
        ret = connect(tcp_socket, results_it->ai_addr, results_it->ai_addrlen);
        if (ret == 0) // Success
        {
            std::cout << results_it << std::endl;
            break;
        }
        std::cout << "ret " << ret << " data" << std::endl;

        ret = -1;
        perror("connect");
        results_it = results_it->ai_next;
    }
    freeaddrinfo(results);
    std::cout << "results " << ret << " data" << std::endl;

    if (ret == -1) {
        handle_error("bind failed");
        return -1;
    }
    ret = listen(tcp_socket, 10);
    if(ret!=0){
        perror("listen");
        return -1;
    }
    ret = convert_ip_port_to_sockaddr_in(ip_string, port_string, &recv_addr);

    if (ret == -1) {
        handle_error("ip/port conversion failed");
    }
    struct ChatMonMsg to_send;
    memset(&to_send, 0, sizeof(to_send));
    to_send.type = MON_CONNECT;
    to_send.nickname_len = 0;
    to_send.data_len = sizeof(nickname);
    char msg_buf[sizeof(struct ChatMonMsg)+strlen(nickname)];
    std::string combmsg;
    std::string typemsg =std::to_string(to_send.type);
    combmsg = (typemsg + nickname);
    int offset = 0;
    memcpy(&msg_buf[offset], &to_send, sizeof(struct ChatMonMsg));
    offset +=sizeof(struct ChatMonMsg);
    memcpy(&msg_buf[offset], nickname, strlen(nickname));
    ret = send(tcp_socket, msg_buf, sizeof(msg_buf), 0);
    std::cout << "msg_buf now contains " << msg_buf << " data" << std::endl;

//    std::cout << "ret 1 " << ret << std::endl;
//    ret = send(tcp_socket, &to_send, sizeof(to_send), 0);
    std::cout << "ret 2 " << ret << std::endl;

    std::cout << "sending " << to_send.type << std::endl;
    if (ret <= 0) {
        handle_error(" Send failed");
        close(tcp_socket);
        closedsocket = true;
        return 1;
    }
    // Check if send worked, clean up and exit if not.


    // Placeholder for messages received from the server
    struct ChatMonMsg server_message;
    // After sending the connect monitor message, the monitor will just
    // sit and wait for messages to output.





    while (stop == false) {
        printf("Waiting on poll()...\n");
        nfds = 0;
        fds[nfds].fd = tcp_socket;
        fds[nfds].events=POLLIN;
        nfds+=1;

        for (int i = 0; i < client_list.size(); i++) {
            if (client_list[i] != NULL) {
                fds[nfds].fd = client_list[i]->client_socket;
                fds[nfds].events = POLLIN;
                if (client_list[i]->bytes_to_send > 0) {
                    fds[nfds].events |= POLLOUT;
                    //poll_fds[num_fds].events = POLLIN | POLLOUT;
                }
                client_list[i]->poll_index = nfds;
                nfds += 1;
            }
        }

        ret = poll(fds, nfds, 50000);
        if (ret < 0) {
            perror("poll");
            break;
        }

        std::cout << "Poll returned " << ret << " ready file descriptors." << std::endl;
        if (ret == 0)
            continue;
        if (fds[0].revents & POLLIN) {
            std::cout << "tcp_socket ready to accept new connection" << std::endl;
            // 4. accept new connection on socket
            client_address_len = sizeof(sockaddr_in);
            new_client_socket = accept(tcp_socket, (struct sockaddr *) &client_address, &client_address_len);

            if (new_client_socket == -1) {
                perror("accept");
                continue;
            }

            temp_client = (struct Client *) malloc(sizeof(struct Client));
            temp_client->client_socket = new_client_socket;
            temp_client->bytes_received = 0;
            temp_client->bytes_to_send = 0;
            memcpy(&temp_client->client_address, &client_address, client_address_len);
            temp_client->client_address_len = client_address_len;
            client_list.push_back(temp_client);

            std::cout << "Accepted connection from " <<
                      printable_address((struct sockaddr_storage *) &client_address, client_address_len) << std::endl;
            continue;
        }

        current_size = nfds;
        for (int i = 0; i < client_list.size(); i++) {
            if (client_list[i] == NULL)
                continue;

            if (fds[client_list[i]->poll_index].revents & POLLIN) {
                std::cout << "client " << i << " ready to recv data" << std::endl;
                // 5. Receive data
                ret = recv(client_list[i]->client_socket, &client_list[i]->recv_buf[client_list[i]->bytes_received],
                           2047, 0);

                if (ret <= 0) {
                    handle_error("recv failed for some reason");
                    close(client_list[i]->client_socket);
                    free(client_list[i]);
                    client_list[i] = NULL;
                    continue;
                }

                // Original server code, just print out the raw bytes received.
                std::cout << "Received " << ret << " bytes of data." << std::endl;
                client_list[i]->bytes_received += ret;
            }
        }
        for (int i = 0; i < client_list.size(); i++) {
            if (client_list[i] == NULL)
                continue;

            if (client_list[i]->bytes_received > 0) {
                ret = process_data(client_list[i]);
                if (ret < 0) {
                    std::cout << "Error processing data!" << std::endl;
                }
            }
        }
        for (int i = 0; i < client_list.size(); i++) {
            if (client_list[i] == NULL)
                continue;

            if (fds[client_list[i]->poll_index].revents & POLLOUT) {
                std::cout << "client " << i << " ready to send data" << std::endl;
                // 6. Send back data
                ret = send(client_list[i]->client_socket, client_list[i]->send_buf, client_list[i]->bytes_to_send, 0);

                if (ret <= 0) {
                    handle_error("send failed for some reason");
                    close(client_list[i]->client_socket);
                    free(client_list[i]);
                    client_list[i] = NULL;
                    continue;
                }

                std::cout << "sent " << ret << " bytes to client" << std::endl;
                if (ret == client_list[i]->bytes_to_send) {
                    client_list[i]->bytes_to_send = 0;
                } else {
                    std::cout << "Uh oh, didn't send all the bytes!" << std::endl;
                }
            }
        }
    }
    struct ChatMonMsg disconnect_message;
    memset(&disconnect_message, 0, sizeof(disconnect_message));
    disconnect_message.type = MON_DISCONNECT;
    disconnect_message.nickname_len = 0;
    disconnect_message.data_len = sizeof(nickname);

    std::cout << "ret  " << ret << std::endl;

    ret = send(tcp_socket, &disconnect_message, sizeof(disconnect_message), 0);
    std::cout << "ret  " << ret << std::endl;

    if (ret <= 0) {
        handle_error("client Send failed");
        close(tcp_socket);
        closedsocket = true;
        return 1;
    }

    std::cout << "Shut down message sent to server, exiting!\n";

    close(tcp_socket);
    closedsocket = true;
    return 0;
}
