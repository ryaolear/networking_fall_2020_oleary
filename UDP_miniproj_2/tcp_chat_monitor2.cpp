//
// Created by Nathan Evans
//
#include <iostream>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/poll.h>
#include <netdb.h>
#include "tcp_utils.h"
#include "tcp_chat.h"
#include <sys/time.h>
#include <sys/ioctl.h>


// Variable used to shut down the monitor when ctrl+c is pressed.
static bool stop = false;

// Handler for when ctrl+c is pressed.
// Just set the global 'stop' to true to shut down the server.
void handle_ctrl_c(int the_signal) {
    std::cout << "Handled sigint\n";
    stop = true;
}

/**
 * TCP chat monitor. Connects to a chat server and
 * simply prints out data to the client until it quits.
 *
 * e.g., ./tcpchatmon 127.0.0.1 8888
 *
 * @param argc count of arguments on the command line
 * @param argv array of command line arguments
 * @return 0 on success, non-zero if an error occurred
 */
int main(int argc, char *argv[]) {
    int len, rc, on = 1;
    int listen_sd = -1, new_sd = -1;
    int desc_ready, end_server = 0, compress_array = 0;
    int close_conn;
    char buffer[80];
    struct sockaddr_in6 addr;
//    int    timeout;
    struct pollfd fds[200];
    int nfds = 1, current_size = 0, i, j;
    // Alias for argv[1] for convenience
    char *ip_string;
    // Alias for argv[2] for convenience
    char *port_string;
    // Alias for argv[3] for convenience
    char *nickname;
    // The socket used to connect/send/recv data to the TCP chat server
    int tcp_socket;
    // Variable used to check return codes from various functions
    int ret;
    struct pollfd pfds[200];
    // IPv4 structure representing and IP address and port of the destination
    struct sockaddr_in dest_addr;
    socklen_t dest_addr_len;
    char recv_buf[2048];
    struct sockaddr_in recv_addr;

    socklen_t recv_addr_size;
    int timeout = 60000;

    bool closedsocket = true;


    int new_recvr_socket;
    // Set dest_addr to all zeroes, just to make sure it's not filled with junk
    // Note we could also make it a static variable, which will be zeroed before execution

    // Signal handler to deal with quitting the program appropriately
    struct sigaction ctrl_c_handler;
    ctrl_c_handler.sa_handler = handle_ctrl_c;
    sigemptyset(&ctrl_c_handler.sa_mask);
    ctrl_c_handler.sa_flags = 0;
    sigaction(SIGINT, &ctrl_c_handler, NULL);

    // Note: this needs to be 3, because the program name counts as an argument!
    if (argc < 3) {
        std::cerr << "Please specify server HOST PORT [NICKNAME] as arguments1." << std::endl;

        return 1;
    }
    std::string test;

    nickname = NULL;

    // Indicates that a nickname was provided for the monitor (for direct messages)
    if (argc == 4) {
        nickname = argv[3];
    }

    // Set up variables "aliases"
    ip_string = argv[1];
    port_string = argv[2];

    std::cout << nickname << std::endl;
    // Create the TCP socket.
    // AF_INET is the address family used for IPv4 addresses
    // SOCK_STREAM indicates creation of a TCP socket
    tcp_socket = socket(AF_INET, SOCK_STREAM, 0);
    closedsocket = false;
//    if (tcp_socket < 0) {
//        perror("socket() failed");
//        exit(-1);
//    }
//    rc = setsockopt(tcp_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on));
//    if (rc < 0)
//    {
//        perror("setsockopt() failed");
//        close(tcp_socket);
//        exit(-1);
//    }
//    rc = ioctl(tcp_socket, FIONBIO, (char*)&on);
//    if (rc < 0)
//    {
//        perror("ioctl() failed");
//        close(tcp_socket);
//        exit(-1);
//    }

    // TODO: build a chat client message of type MON_CONNECT
    //       if a nickname was provided, include that in the message as well
//    std::cout << "tcp socket " << tcp_socket << std::endl;
    struct addrinfo hints;
    struct addrinfo *results;
    struct addrinfo *results_it;
    memset(&hints, 0, sizeof(struct addrinfo));
    // TODO: Get the IP address in binary format using get getaddrinfo!

    hints.ai_family = AF_INET;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_socktype = SOCK_STREAM;
    ret = getaddrinfo(ip_string, port_string, &hints, &results);
    if (ret != 0) {
        perror("getaddrinfo");
        return -1;
    }
//    rc = bind(tcp_socket,
//              (struct sockaddr *)&dest_addr, sizeof(dest_addr));
//    if(rc < 0)
//    {
//        perror("bind() failed");
//        close(tcp_socket);
//        exit(-1);
//    }
    results_it = results;
//    rc = listen(tcp_socket, 32);
//    if (rc < 0)
//    {
//        perror("listen() failed");
//        close(tcp_socket);
//        exit(-1);
//    }
//    memset(fds, 0 , sizeof(fds));
//    fds[0].fd = tcp_socket;
//    fds[0].events = POLLIN;

    ret = -1;
    while (results_it != NULL) {
        std::cout << "Attempting to connect to " <<
                  printable_address((struct sockaddr_storage *) results_it->ai_addr, results_it->ai_addrlen)
                  << std::endl;
        ret = connect(tcp_socket, results_it->ai_addr, results_it->ai_addrlen);
        if (ret == 0) // Success
        {
            std::cout << results_it << std::endl;
            break;
        }
        ret = -1;
        perror("connect");
        results_it = results_it->ai_next;
    }
    freeaddrinfo(results);

    if (ret == -1) {
        handle_error("connect failed");
    }


    ret = convert_ip_port_to_sockaddr_in(ip_string, port_string, &dest_addr);

    if (ret == -1) {
        handle_error("ip/port conversion failed");
    }

    struct ChatMonMsg to_send;
    memset(&to_send, 0, sizeof(to_send));
    to_send.type = MON_CONNECT;
    to_send.nickname_len = 0;
    to_send.data_len = sizeof(nickname);
    char msg_buf[to_send.data_len+to_send.nickname_len+1];
    std::string combmsg;
    std::string typemsg =std::to_string(to_send.type);
    combmsg = (typemsg + nickname);
    strcpy(msg_buf, combmsg.c_str());

    ret = send(tcp_socket, msg_buf, sizeof(msg_buf), 0);
    std::cout << "msg_buf now contains " << msg_buf << " data" << std::endl;

//    std::cout << "ret 1 " << ret << std::endl;
    ret = send(tcp_socket, &to_send, sizeof(to_send), 0);
//    std::cout << "ret 2 " << ret << std::endl;

    std::cout << "sending " << to_send.type << "code" << std::endl;
    if (ret <= 0) {
        handle_error(" Send failed");
        close(tcp_socket);
        closedsocket = true;
        return 1;
    }
    // TODO: send the MON_CONNECT message to the server
    // Check if send worked, clean up and exit if not.


    // Placeholder for messages received from the server
    struct ChatMonMsg server_message;
    // After sending the connect monitor message, the monitor will just
    // sit and wait for messages to output.
    ssize_t rcread;
    char buff[100];
    while (stop == false) {
        printf("Waiting on poll()...\n");
//        rc = poll(fds, nfds, timeout);
        std::cout<< fds <<std::endl;
        std::cout<< rc <<std::endl;
        rc = poll(fds, 1, -1);
        std::cout<< rc <<std::endl;

        rcread = read(0, buff, 99);
        std::cout<< rc << " " <<  errno << " " << strerror(errno) << " "<< buff<<std::endl;
        if (rc < 0) {
            perror("  poll() failed");
            break;
        }
        if (rc == 0) {
            printf("  poll() timed out.  End program.\n");
            break;
        }
        current_size = nfds;
        for (i = 0; i < current_size; i++) {
            if (fds[i].revents == 0)
                continue;
            if (fds[i].revents != POLLIN) {
                printf("  Error! revents = %d\n", fds[i].revents);
                end_server = 1;
                break;

            }
            if (fds[i].fd == tcp_socket) {
            }
            ret = recv(tcp_socket, recv_buf, 2047, 0);
            std::cout << "ret 1  " << ret << std::endl;

            if (ret <= 0) {
                perror("listen failed");
                close(tcp_socket);
                closedsocket = true;
                return 1;
            }
        }

        if (closedsocket == false) {

            ret = recv(tcp_socket, recv_buf, 2047, 0);
            std::cout << "ret 2  " << ret << std::endl;

            if (ret <= 0) {
                perror("listen failed");
                close(tcp_socket);
                closedsocket = true;
                return 1;
            }
//        ret = recv(incoming_socket, recv_buf, 2047, 0);
            recv_buf[ret] = '\0';
            std::cout << "Data received: " << recv_buf << std::endl;

            std::cout << "recvfrom  " << &dest_addr << std::endl;
            if (ret <= 0) {

                perror("recv");
                std::cout << "ret  " << ret << std::endl;


                handle_error("recv failed for some reason");
                close(tcp_socket);
                closedsocket = true;

                return -1;
            }
        }




        // TODO: receive messages from the server
        //       when a message from the server is received, you should determine its type and data, then print
        //       out the chat message to the screen, including the nickname of the sender
        // TODO: read from stdin, in case the user types 'quit'
    }
    struct ChatMonMsg disconnect_message;
    memset(&disconnect_message, 0, sizeof(disconnect_message));
    disconnect_message.type = MON_DISCONNECT;
    disconnect_message.nickname_len = 0;
    disconnect_message.data_len = sizeof(nickname);

    std::cout << "ret  " << ret << std::endl;

    ret = send(tcp_socket, &disconnect_message, sizeof(disconnect_message), 0);
    std::cout << "ret  " << ret << std::endl;

    if (ret <= 0) {
        handle_error("client Send failed");
        close(tcp_socket);
        closedsocket = true;
        return 1;
    }

    // TODO: build and send a MON_DISCONNECT message to let the server know this monitor has gone away
    std::cout << "Shut down message sent to server, exiting!\n";

    close(tcp_socket);
    closedsocket = true;
    return 0;
}
